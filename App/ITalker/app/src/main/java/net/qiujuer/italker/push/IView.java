package net.qiujuer.italker.push;

/**
 * @author qiujuer Email:qiujuer@live.cn
 * @version 1.0.0
 * IView --> 界面输出
 */
public interface IView {
    String getInputString();

    void setResultString(String string);
}
