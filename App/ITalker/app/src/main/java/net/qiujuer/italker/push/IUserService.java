package net.qiujuer.italker.push;

/**
 * @author qiujuer Email:qiujuer@live.cn
 * @version 1.0.0
 * IUserService --> 具体的查询动作
 */
public interface IUserService {
    String search(int hashCode);
}
