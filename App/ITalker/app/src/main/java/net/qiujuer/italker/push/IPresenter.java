package net.qiujuer.italker.push;

/**
 * @author qiujuer Email:qiujuer@live.cn
 * @version 1.0.0
 * Presenter-->查询逻辑
 */
public interface IPresenter {
    void search();
}
