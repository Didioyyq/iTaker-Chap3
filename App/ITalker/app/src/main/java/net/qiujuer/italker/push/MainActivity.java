package net.qiujuer.italker.push;

import android.widget.EditText;
import android.widget.TextView;

import net.qiujuer.italker.common.app.Activity;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends Activity implements IView {
    //布局通过Butterknife绑定到对象或方法上
    //绑定注解, 注解在res文件下的某xml文件中, type是id, value是txt_result
    @BindView(R.id.txt_result)
    TextView mResultText;

    @BindView(R.id.edit_query)
    EditText mInputText;

    private IPresenter mPresenter;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData() {
        super.initData();
        mPresenter = new Presenter(this);
    }

    //注解绑定布局到onSubmit()方法, 一旦OnClick监听到btn_submit事件, 就触发onSubmit方法
    @OnClick(R.id.btn_submit)
    void onSubmit() {
        mPresenter.search();
    }

    @Override
    public String getInputString() {
        return mInputText.getText().toString();
    }

    @Override
    public void setResultString(String string) {
        mResultText.setText(string);
    }
}
